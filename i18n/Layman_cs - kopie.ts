<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="cs_CZ" sourcelanguage="en">
<context>
    <name>DialogBase</name>
    <message>
        <location filename="../dlg_addLayer.ui" line="35"/>
        <source>Layman - Layers</source>
        <translation>Layman - Vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="47"/>
        <source>Load WMS</source>
        <translation>Načíst WMS</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="64"/>
        <source>Layers on Layman</source>
        <translation>Vrstvy v Laymanu</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="60"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="104"/>
        <source>Preview:</source>
        <translation>Náhled:</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="120"/>
        <source>More info</source>
        <translation>Více info</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="133"/>
        <source>Load WFS</source>
        <translation>Načíst WFS</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="146"/>
        <source>Delete layer</source>
        <translation>Smazat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="29"/>
        <source>Layman</source>
        <translation>Layman</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="47"/>
        <source>Login</source>
        <translation>Přihlásit se</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="73"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="86"/>
        <source>Login:</source>
        <translation>Email:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="106"/>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="35"/>
        <source>Layman - Create Composite</source>
        <translation>Layman - Vytvořit mapovou komozici</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="47"/>
        <source>Create </source>
        <translation>Vytvořit</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="60"/>
        <source>Storno</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="74"/>
        <source>Layer</source>
        <translation>Vrstva</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="88"/>
        <source>You can choose externt of composition from layer:</source>
        <translation>Můžete zvolit prostorový rozsah z vrstvy:</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="101"/>
        <source>Composition name:</source>
        <translation>Jméno kompozice:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="132"/>
        <source>Title:</source>
        <translation>Název:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="292"/>
        <source>Extent of canvas:</source>
        <translation>Prostorový rozsah dat:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="279"/>
        <source>XMin:</source>
        <translation>XMin:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="266"/>
        <source>XMax:</source>
        <translation>XMax:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="315"/>
        <source>YMin:</source>
        <translation>YMin:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="253"/>
        <source>YMax:</source>
        <translation>YMax:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="106"/>
        <source>Description:</source>
        <translation>Popis:</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="275"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Composition name already exists!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Název kompozice již existuje!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dlg_createComposite.ui" line="288"/>
        <source>Canvas extent</source>
        <translation>Rozsah dat</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="35"/>
        <source>Layman - Edit metadata</source>
        <translation>Layman - editovat metadata</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="47"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="119"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="145"/>
        <source>Units:</source>
        <translation>Jednotka:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="158"/>
        <source>Scale:</source>
        <translation>Měřítko:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="171"/>
        <source>User:</source>
        <translation>Uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="35"/>
        <source>Layman - Export Layer to server</source>
        <translation>Layman - Export vrstvy na server</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="57"/>
        <source>Export layer</source>
        <translation>Exportovat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="95"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="41"/>
        <source>Layman -Manage Maps</source>
        <translation>Layman - Správce mapových kompozic</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="66"/>
        <source>Layers</source>
        <translation>Vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="97"/>
        <source>Existing layers in map composition:</source>
        <translation>Existující vrstvy v mapové kompozici:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="110"/>
        <source> Delete Layer</source>
        <translation>Smazat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="143"/>
        <source>Layer preview:</source>
        <translation>Náhled vrstvy:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="156"/>
        <source>Processing data:</source>
        <translation>Probíhá nahrávání:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="324"/>
        <source> Add Layer</source>
        <translation>Přidat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="218"/>
        <source>Loaded vector layer from canvas:</source>
        <translation type="obsolete">Vektorové vrstvy v QGIS:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="258"/>
        <source>Existing layers in Layman:</source>
        <translation>Existující vrstvy na serveru:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="271"/>
        <source>Up</source>
        <translation>Nahoru</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="284"/>
        <source>Down</source>
        <translation>Dolů</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="297"/>
        <source>Save order</source>
        <translation>Uložit pořadí</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="381"/>
        <source>Map compositions</source>
        <translation>Mapové kompozice:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="406"/>
        <source>Existing map compositions:</source>
        <translation>Existující mapové kompozice:</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="419"/>
        <source> Delete Map</source>
        <translation>Smazat mapu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="436"/>
        <source> Add Map</source>
        <translation>Přidat Mapu</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="449"/>
        <source>  Edit metada</source>
        <translation>Upravit metadata</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="462"/>
        <source>Load Map (WMS)</source>
        <translation>Načíst kompozici (WMS)</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="485"/>
        <source>Load Map (WFS)</source>
        <translation>Načíst kompozici (WFS)</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="35"/>
        <source>Layman - Add Map</source>
        <translation>Layman - Načíst mapovou komozici</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="47"/>
        <source>Load map (WMS)</source>
        <translation>Načíst kompozici (WMS)</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="117"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Unable to load already loaded composition!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Nelze načíst již načtenou kompozici!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="130"/>
        <source>Load map (WFS)</source>
        <translation>Načíst kompozici (WFS)</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="41"/>
        <source>Layman username:</source>
        <translation>Layman uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="54"/>
        <source>Liferay username:</source>
        <translation>Liferay uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="93"/>
        <source>Logout</source>
        <translation>Odhlásit se</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="143"/>
        <source>Loading data</source>
        <translation>Načítání dat</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="241"/>
        <source> Add to composite</source>
        <translation>Přidat do kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="218"/>
        <source>QGIS map list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="341"/>
        <source>WMS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="354"/>
        <source>WMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_importMap.ui" line="367"/>
        <source>WFS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="175"/>
        <source>Load composition</source>
        <translation>Načíst kompozici</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="132"/>
        <source>Plugin version:</source>
        <translation>Verze pluginu</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="191"/>
        <source>Set permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="170"/>
        <source>Install dependencies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="158"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="171"/>
        <source>Available version:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Layman</name>
    <message>
        <location filename="../Layman.py" line="2141"/>
        <source>&amp;Layman</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Layman.py" line="300"/>
        <source>Current Row Number</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Layman.py" line="306"/>
        <source>Login</source>
        <translation>Přihlášení</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="315"/>
        <source>Save as to JSON and SLD</source>
        <translation>Uložit jako JSON se symbologií</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="324"/>
        <source>Load from JSON</source>
        <translation>Načíst z JSON</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="333"/>
        <source>Export layer to server</source>
        <translation>Exportovat vrstvu na server</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="340"/>
        <source>Load layer from server</source>
        <translation>Načíst vrstvu ze serveru</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="347"/>
        <source>Manage maps</source>
        <translation>Správce mapových kompozic</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="354"/>
        <source>Load map from server</source>
        <translation>načíst mapovou komopozici ze serveru</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="376"/>
        <source>User info</source>
        <translation>Uživatelské info</translation>
    </message>
</context>
</TS>
