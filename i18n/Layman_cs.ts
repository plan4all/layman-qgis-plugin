<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="cs_CZ" sourcelanguage="en">
<context>
    <name>AddLayerDialog</name>
    <message>
        <location filename="../dlg_addLayer.ui" line="35"/>
        <source>Layman - Layers cataloque</source>
        <translation>Layman - Katalog vrstev</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="47"/>
        <source>Load WMS</source>
        <translation>Načíst WMS</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="495"/>
        <source>Layer</source>
        <translation>Vrstva</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="500"/>
        <source>Owner</source>
        <translation>Vlastník</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="505"/>
        <source>Permissions</source>
        <translation>Práva</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="64"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="93"/>
        <source>Preview:</source>
        <translation>Náhled:</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="106"/>
        <source>More info</source>
        <translation>Více info</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="119"/>
        <source>Load WFS</source>
        <translation>Načíst WFS</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="136"/>
        <source>Delete layer</source>
        <translation>Smazat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="153"/>
        <source>Loading data</source>
        <translation>Načítání dat</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="185"/>
        <source>Set permissions</source>
        <translation>Nastavit práva</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="554"/>
        <source>Show only my layers</source>
        <translation>Zobrazit jenom mé vrstvy</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../dlg_addLayer.ui" line="202"/>
        <source>Nepřihlášený uživatel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="215"/>
        <source>Layer URL (copy to clipboard)</source>
        <translation>URL vrstvy (uložit do schránky)</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="227"/>
        <source>WMS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="244"/>
        <source>WFS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="262"/>
        <source>Disable</source>
        <translation>Vypnout</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="275"/>
        <source>Load Postgis</source>
        <translation>Načíst Postgis</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="305"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="404"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="448"/>
        <source>Write:</source>
        <translation>Zápis:</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="364"/>
        <source>Read:</source>
        <translation>Čtení:</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="471"/>
        <source>Remove</source>
        <translation>Odebrat</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="510"/>
        <source>CRS</source>
        <translation type="unfinished">CRS</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="538"/>
        <source>Filter</source>
        <translation>Filtrovat</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="515"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="322"/>
        <source>Back to layers</source>
        <translation>Zpět na vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="339"/>
        <source>Layer permissions</source>
        <translation>Práva vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_addLayer.ui" line="351"/>
        <source>Available users:</source>
        <translation>Dostupní uživatelé</translation>
    </message>
</context>
<context>
    <name>AddMapDialog</name>
    <message>
        <location filename="../dlg_addMap.ui" line="35"/>
        <source>Layman - Map compositions cataloque</source>
        <translation>Layman - Katalog mapových kompozic</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="47"/>
        <source>Load map (WMS)</source>
        <translation>Načíst kompozici (WMS)</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="60"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="447"/>
        <source>Compositions</source>
        <translation>Kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="452"/>
        <source>Owner</source>
        <translation>Vlastník</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="457"/>
        <source>Permissions</source>
        <translation>Práva</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="89"/>
        <source>Preview:</source>
        <translation>Náhled:</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="102"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Unable to load already loaded composition!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Nelze načíst již načtenou kompozici!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="115"/>
        <source>Load map (WFS)</source>
        <translation>Načíst kompozici (WFS)</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="128"/>
        <source>Loading data</source>
        <translation>Načítání dat</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="166"/>
        <source>Load composition</source>
        <translation>Načíst kompozici</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="183"/>
        <source>Delete map</source>
        <translation>Smazat kompozici</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="200"/>
        <source>Set permissions</source>
        <translation>Nastavit práva</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="514"/>
        <source>Show only my compositions</source>
        <translation>Zobrazit jenom mé kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="217"/>
        <source>Copy URL</source>
        <translation>Kopírovat URL</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="234"/>
        <source>Disable</source>
        <translation>Vypnout</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="342"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="409"/>
        <source>Remove</source>
        <translation>Odebrat</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="427"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="302"/>
        <source>Read:</source>
        <translation>Čtení:</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="386"/>
        <source>Write:</source>
        <translation>Zápis:</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="462"/>
        <source>CRS</source>
        <translation>CRS</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="485"/>
        <source>Filter</source>
        <translation>Filtrovat</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="260"/>
        <source>Back to maps</source>
        <translation>Zpět na mapy</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="277"/>
        <source>Map permissions</source>
        <translation>Práva mapy</translation>
    </message>
    <message>
        <location filename="../dlg_addMap.ui" line="289"/>
        <source>Available users:</source>
        <translation>Dostupní uživatelé</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../dlg_addMap.ui" line="501"/>
        <source>Nepřihlášený uživatel</source>
        <translation>Not logged in user</translation>
    </message>
</context>
<context>
    <name>ConnectionManagerDialog</name>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="35"/>
        <source>Layman</source>
        <translation>Layman</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="47"/>
        <source>Login</source>
        <translation>Přihlásit se</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="64"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="77"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="90"/>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="126"/>
        <source>Logout</source>
        <translation>Odhlásit se</translation>
    </message>
    <message>
        <location filename="../dlg_ConnectionManager.ui" line="143"/>
        <source>Continue without login</source>
        <translation>Pokračovat bez přihlášení</translation>
    </message>
</context>
<context>
    <name>CurrentCompositionDialog</name>
    <message>
        <location filename="../dlg_currentComposition.ui" line="29"/>
        <source>Layman - Current composition</source>
        <translation>Layman - Aktuální kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="41"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="54"/>
        <source>New</source>
        <translation>Nová kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="71"/>
        <source>Composition is read only</source>
        <translation>Kompozice je pouze ke čtení</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="84"/>
        <source>  Edit metadata</source>
        <translation>Upravit metadata</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1374"/>
        <source>Save changes</source>
        <translation>Uložit změny</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="143"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="160"/>
        <source>WMS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="173"/>
        <source>Set permissions</source>
        <translation>Nastavit práva</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="263"/>
        <source>Check all layers</source>
        <translation>Vybrat vše</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="277"/>
        <source>Layer</source>
        <translation>Vrstva</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="282"/>
        <source>Server type</source>
        <translation>Služba</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="287"/>
        <source>Action</source>
        <translation>Akce</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="292"/>
        <source>Atributes</source>
        <translation>Atributy</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="190"/>
        <source>Exporting raster</source>
        <translation>Export rastru</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="203"/>
        <source>Export to QField</source>
        <translation>Exportovat do QField</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="220"/>
        <source>Copy URL</source>
        <translation>Kopírovat URL</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="237"/>
        <source>Info</source>
        <translation type="unfinished">Info</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1041"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="390"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1095"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="457"/>
        <source>Remove</source>
        <translation>Odebrat</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="350"/>
        <source>Read:</source>
        <translation>Čtení:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="434"/>
        <source>Write:</source>
        <translation>Zápis:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1014"/>
        <source>XMax:</source>
        <translation>XMax:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="958"/>
        <source>YMin:</source>
        <translation>YMin:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="603"/>
        <source>Canvas extent</source>
        <translation>Rozsah dat</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="590"/>
        <source>Extent of canvas:</source>
        <translation>Prostorový rozsah dat:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1001"/>
        <source>YMax:</source>
        <translation>YMax:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1228"/>
        <source>Title:</source>
        <translation>Název:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="537"/>
        <source>Get extent of composition from layer</source>
        <translation>Získat prostorový rozsah z vrstvy</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="824"/>
        <source>Description:</source>
        <translation>Popis:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="620"/>
        <source>Create </source>
        <translation>Vytvořit</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1027"/>
        <source>XMin:</source>
        <translation>XMin:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="936"/>
        <source>Extent of composition</source>
        <translation>Prostorový rozsah</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="760"/>
        <source>Metadata</source>
        <translation type="unfinished">Metadata</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="772"/>
        <source>Scale:</source>
        <translation>Měřítko:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="811"/>
        <source>User:</source>
        <translation>Uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="873"/>
        <source>Units:</source>
        <translation>Jednotka:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="912"/>
        <source>EPSG:</source>
        <translation>EPSG:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1058"/>
        <source>From canvas</source>
        <translation>Z plátna</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1075"/>
        <source>From capatibilites</source>
        <translation>Z vrstev</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1163"/>
        <source>Max scale:</source>
        <translation>Maximální měřítko:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1241"/>
        <source>Opacity:</source>
        <translation>Průhlednost:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1150"/>
        <source>Grey scale:</source>
        <translation>Odstíny šedi:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1332"/>
        <source>Visibility:</source>
        <translation>Viditelnost:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1254"/>
        <source>Group:</source>
        <translation>Skupina:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1267"/>
        <source>title</source>
        <translation>Titulek</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1345"/>
        <source>Min scale:</source>
        <translation>Minimální měřítko</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1319"/>
        <source>Single tile:</source>
        <translation>Tiled/Dlaždicovat:</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1189"/>
        <source>Base layer:</source>
        <translation>Podkladová vrstva</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="325"/>
        <source>Map permissions</source>
        <translation>Práva mapy</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="337"/>
        <source>Available users:</source>
        <translation>Dostupní uživatelé</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="524"/>
        <source>New composition</source>
        <translation>Nová mapová kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="748"/>
        <source>Edit map composition</source>
        <translation>Editovat mapovou kompozici</translation>
    </message>
    <message>
        <location filename="../dlg_currentComposition.ui" line="1112"/>
        <source>Layer properties</source>
        <translation>Vlastnosti vrstvy</translation>
    </message>
</context>
<context>
    <name>DialogBase</name>
    <message>
        <location filename="../dlg_editMap.ui" line="35"/>
        <source>Layman - Edit metadata</source>
        <translation>Layman - editovat metadata</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="47"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="60"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="73"/>
        <source>Extent of composition</source>
        <translation>Prostorový rozsah</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="85"/>
        <source>From canvas</source>
        <translation>Z plátna</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="108"/>
        <source>YMin:</source>
        <translation>YMin:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="151"/>
        <source>From capatibilites</source>
        <translation>Z vrstev</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="164"/>
        <source>YMax:</source>
        <translation>YMax:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="177"/>
        <source>XMax:</source>
        <translation>XMax:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="190"/>
        <source>XMin:</source>
        <translation>XMin:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="204"/>
        <source>Metadata</source>
        <translation type="unfinished">Metadata</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="216"/>
        <source>Scale:</source>
        <translation>Měřítko:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="255"/>
        <source>User:</source>
        <translation>Uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="268"/>
        <source>Description:</source>
        <translation>Popis:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="281"/>
        <source>Title:</source>
        <translation>Název:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="317"/>
        <source>Units:</source>
        <translation>Jednotka:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="330"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../dlg_editMap.ui" line="369"/>
        <source>EPSG:</source>
        <translation>EPSG:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="29"/>
        <source>Layman</source>
        <translation>Layman</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="41"/>
        <source>Update</source>
        <translation>Aktualizovat</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="71"/>
        <source>Get older version &lt; 3.24</source>
        <translation>Získat starší verzi &lt; 3.24</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="87"/>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="94"/>
        <source>Layman username:</source>
        <translation>Layman uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="101"/>
        <source>Wagtail username:</source>
        <translation>Wagtail uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="108"/>
        <source>Plugin version:</source>
        <translation>Verze pluginu</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="115"/>
        <source>Layman version:</source>
        <translation>Verze Laymana:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="122"/>
        <source>Available version:</source>
        <translation>Dostupná verze:</translation>
    </message>
    <message>
        <location filename="../dlg_userInfo.ui" line="192"/>
        <source>Port:</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="35"/>
        <source>Layman - Browse metadata cataloque</source>
        <translation>Layman - Prohlížet metadatový katalog</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="47"/>
        <source>Load map (WMS)</source>
        <translation>Načíst kompozici (WMS)</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="74"/>
        <source>Compositions</source>
        <translation>Kompozice</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="88"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Unable to load already loaded composition!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; color:#ff0000;&quot;&gt;Nelze načíst již načtenou kompozici!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="101"/>
        <source>Load map (WFS)</source>
        <translation>Načíst kompozici (WFS)</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="114"/>
        <source>Loading data</source>
        <translation>Načítání dat</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="146"/>
        <source>Load composition</source>
        <translation>Načíst kompozici</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="159"/>
        <source>Query:</source>
        <translation>Dotaz:</translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="172"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="185"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dlg_addMicka.ui" line="198"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
</context>
<context>
    <name>ImportLayerDialog</name>
    <message>
        <location filename="../dlg_importLayer.ui" line="35"/>
        <source>Layman - Export Layer to server</source>
        <translation>Layman - Export vrstvy na server</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="356"/>
        <source>Export layer</source>
        <translation>Exportovat vrstvu</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="64"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="194"/>
        <source>Layer</source>
        <translation>Vrstva</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="199"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="102"/>
        <source>Processing data:</source>
        <translation>Probíhá nahrávání:</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="128"/>
        <source>Copy error log</source>
        <translation>Kopírovat log</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="145"/>
        <source>Raster resampling method:</source>
        <translation>Vzorkování rastru:</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="215"/>
        <source>Export time series</source>
        <translation>Exportovat</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="227"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="240"/>
        <source>Regex:</source>
        <translation>Regex:</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="341"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="282"/>
        <source>Login to database</source>
        <translation>Přihlásit se do databáze</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="297"/>
        <source>Password:</source>
        <translation>Heslo:</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="310"/>
        <source>Username:</source>
        <translation>Uživatel:</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.ui" line="323"/>
        <source>Confirm</source>
        <translation>Potvrdit</translation>
    </message>
    <message>
        <location filename="../dlg_importLayer.py" line="329"/>
        <source>Sucessfully exported: </source>
        <translation>Úspěšně exportováno: </translation>
    </message>
</context>
<context>
    <name>Layman</name>
    <message>
        <location filename="../Layman.py" line="2172"/>
        <source>&amp;Layman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Layman.py" line="304"/>
        <source>Current Row Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Layman.py" line="309"/>
        <source>Login</source>
        <translation>Přihlásit se</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="318"/>
        <source>Save as JSON and QML</source>
        <translation>Uloži jako JSON a QML</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="327"/>
        <source>Load from JSON</source>
        <translation>Načíst z JSON</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="336"/>
        <source>Export layer to server</source>
        <translation>Exportovat vrstvu na server</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="343"/>
        <source>Layers cataloque</source>
        <translation>Katalog vrstev</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="350"/>
        <source>Map compositions cataloque</source>
        <translation>Katalog mapových kompozic</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="357"/>
        <source>Current composition</source>
        <translation>Aktuální kompozice</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="371"/>
        <source>User info</source>
        <translation>Informace o uživateli</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="364"/>
        <source>Browse the metadata catalog</source>
        <translation>Prohlížet metadatový katalog</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="3560"/>
        <source>Saving composition</source>
        <translation>Ukládání kompozice</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="4407"/>
        <source>Plugin update</source>
        <translation>Aktualizace pluginu</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="4398"/>
        <source>This version of the plugin is not included in the QGIS repository and may contain new untested functionalities. Do you really want to install this version?</source>
        <translation>Tato verze pluginu není v QGIS repozitáři a může obsahovat nové netestované funkcionality. Chcete opravdu instalovat tuto verzi?</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="4407"/>
        <source>Plugin requires QGIS version 3.26 and higher. Do you still want to continue?</source>
        <translation>Plugin vyžaduje verzi QGIS 3.26 a vyšší. Chcete přesto pokračovat?</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="4453"/>
        <source>Sucessfully exported:</source>
        <translation type="unfinished">Úspěšně exportováno:</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="450"/>
        <source>This project includes link to Layman server. Do you want set the project as current composition?</source>
        <translation>Tento projekt obsahuje odkaz na Layman server. Chcete ho nastavit jako aktuální kompozici?</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="470"/>
        <source>This project includes link to Layman server. Do you want login?</source>
        <translation>Tento projekt obsahuje odkaz na Layman server. Chcete se k tomuto serveru přihlásit?</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="504"/>
        <source>Layer loaded from a local geojson file.</source>
        <translation>Vrstva načtená z lokálního souboru geojson.</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="506"/>
        <source>Layer loaded from a local SHP file.</source>
        <translation>Vrstva načtená z lokálního souboru SHP.</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="508"/>
        <source>Layer stored in QGIS memory. It will be deleted after QGIS is turned off.</source>
        <translation>Layer stored in QGIS memory. It will be deleted after QGIS is turned off.</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="510"/>
        <source>A layer loaded over a WMS service that provides data in a raster format. It is possible to change this service to a WFS vector service using the button.</source>
        <translation>Vrstva načtená přes službu WMS poskytující data v rasterovém formátu. Je možné tuto službu zaměnit za vektorovou službu WFS pomocí tlačítka.</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="512"/>
        <source>A layer loaded over a WFS service that provides data in a vector format. It is possible to change this service to a WMS raster service using the button. Changes in this layer are saved to the server.</source>
        <translation>Vrstva načtená přes službu WFS poskytující data ve vektorovém formátu. Je možné tuto službu zaměnit za rasterovou službu WMS pomocí tlačítka. Změny v této vrstvě jsou ukládány na server.</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="514"/>
        <source>Vector layer loaded from a local file.</source>
        <translation>Vector layer loaded from a local file.</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="888"/>
        <source>Coordinate system was changed to: </source>
        <translation>Souřadnicový systém byl změnen na: </translation>
    </message>
    <message>
        <location filename="../Layman.py" line="888"/>
        <source>. Do you want write it to composition?</source>
        <translation>. Chcete tento souřadnicový systém zapsat do kompozice?</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="1502"/>
        <source>Update layer order</source>
        <translation>Aktualizace pořadí vrstev</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="1557"/>
        <source>Update layer visibility</source>
        <translation>Aktualizace viditelnost vrstev</translation>
    </message>
    <message>
        <location filename="../Layman.py" line="2971"/>
        <source>Sucessfully exported: </source>
        <translation>Úspěšně exportováno: </translation>
    </message>
</context>
</TS>
